# Mahara utils #

> ### Disclaimer: WIP ###
>
> The code in this repo is continous **work in progress** to learn about python and ways to programmatically interact with [Mahara](https://mahara.org/).
> Feel free to use some of it, but take always the **master** branch as a reference.
>
> ***Leave your questions and feedback as new [issues](https://bitbucket.org/alozalv/mahara_utils/issues).***
>
>
> Note that [requests](https://pypi.python.org/pypi/requests) needs to be installed in your system.
>
> Thanks!

Note that [MaharaUploadAPI](https://code.google.com/p/maharadroid/wiki/MaharaUploadAPI) is used, so you need to [allow mobile uploads](https://bitbucket.org/alozalv/mahara_utils/wiki/Allow%20mobile%20uploads) in your Mahara site, as well as [set up a token](https://bitbucket.org/alozalv/mahara_utils/wiki/Set%20up%20upload%20token) for the user whose portfolio will be updated.

You can run *mahara_manager.py* in order to upload a file into a Mahara portfolio. The provided file will be uploaded into a folder named *remote_uploads* under the *Content* section in the portfolio of the user.


```
./mahara_manager.py -h                                                                       
usage: mahara_manager.py [-h] -m MAHARA_SERVER -u USER_NAME -t USER_TOKEN -f FILEPATH

Update a Mahara portfolio by uploading a file.

optional arguments:
  -h, --help            show this help message and exit
  -m MAHARA_SERVER, --mahara MAHARA_SERVER
                        URL where Mahara is available
  -u USER_NAME, --user USER_NAME
                        Name of the user whose portfolio is to be updated
  -t USER_TOKEN, --token USER_TOKEN
                        Token chosen by the user to allow remote updating of
                        his profile
  -f FILEPATH, --file FILEPATH
                        Path to the file to be uploaded

```