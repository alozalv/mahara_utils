#!/usr/bin/env python2.7

'''
This module handles uploading content into Mahara.
MaharaUploadAPI is used to do so.
Check: https://code.google.com/p/maharadroid/wiki/MaharaUploadAPI


'''
import argparse, sys
import requests

UPLOAD_API = "/artefact/file/mobileupload.php"
UPLOAD_FOLDER = "remote_uploads"


def upload_file(mahara_server, user_name, user_token, filepath):
    '''
    Upload the provided file into the suitable student portfolio

    @param mahara_server URL where Mahara is available
    @param user_name Name of the user whose portfolio is to be updated
    @param user_token Token chosen by the user to allow remote updating of his profile
    @param filepath Path to the file to be uploaded
    
    '''


    requests.post(
        mahara_server+UPLOAD_API,
        files={
            'token': (None, user_token),
            'username': (None, user_name),
            'foldername': (None, UPLOAD_FOLDER),
            'userfile': open(filepath, 'rb')})




def parse_arguments():
    '''
    This function parses command line parameters
    '''

    parser = argparse.ArgumentParser(description=\
        "Update a Mahara portfolio by uploading a file.")

    parser.add_argument('-m', '--mahara',
                        action="store",
                        required=True,
                        dest="mahara_server",
                        help="URL where Mahara is available")

    parser.add_argument('-u', '--user',
                        action="store",
                        required=True,
                        dest="user_name",
                        help="Name of the user whose portfolio is to be updated")

    parser.add_argument('-t', '--token',
                        action="store",
                        required=True,
                        dest="user_token",
                        help="Token chosen by the user to allow remote updating of his profile")

    parser.add_argument('-f', '--file',
                        action="store",
                        required=True,
                        dest="filepath",
                        help="Path to the file to be uploaded")


    return parser.parse_args(sys.argv[1:])

if __name__ == '__main__':
    ARGS = parse_arguments()

    upload_file(ARGS.mahara_server, ARGS.user_name, ARGS.user_token, ARGS.filepath)

    print "== DONE! =="


